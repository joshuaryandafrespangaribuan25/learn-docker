package main

import (
	"Containerization-Example/handlers"
	"context"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	// Instantiate new mux router
	router := mux.NewRouter()

	// Set up handlers
	router.HandleFunc("/", handlers.IndexHandler).Methods("GET")
	router.HandleFunc("/generate-feedback", handlers.ProcessGenerateFeedback).Methods("POST")
	router.HandleFunc("/see-result", handlers.ProcessResult).Methods("GET")

	// Instantiate server instance
	server := &http.Server{
		Addr:         ":8080",
		Handler:      router,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	// Run http server on separate process (goroutine)
	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Printf("HTTP server ListenAndServe error: %v\n", err)
		}
	}()

	log.Println("Serving http on port:", server.Addr)

	// Declare wait signal, make program wait for interrupt signal
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	// Shutdown server with timeout context.
	if err := server.Shutdown(ctx); err != nil {
		log.Printf("HTTP server Shutdown error: %v\n", err)
	} else {
		log.Println("Server gracefully stopped")
	}
}
