package handlers

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open("./public/index.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/html")
	w.Write(content)
	log.Println("Rendering index...")
}

func ProcessGenerateFeedback(w http.ResponseWriter, r *http.Request) {
	log.Println("Processing Feedback...")
	err := r.ParseForm()
	if err != nil {
		http.Error(w, "Error Parsing Form", http.StatusBadRequest)
		return
	}

	// Get the value
	feedbackText := r.FormValue("feedback-data")
	log.Println("Data:", feedbackText)

	if _, err := os.Stat("./feedback-folder"); os.IsNotExist(err) {
		log.Println("Creating feedback-folder...")
		err := os.Mkdir("feedback-folder", 0755)
		if err != nil {
			http.Error(w, "Error creating folder", http.StatusInternalServerError)
			return
		}
	} else if err != nil {
		http.Error(w, "Error checking folder", http.StatusInternalServerError)
		return
	} else {
		log.Println("Folder already exist...")
	}

	file, err := os.Create("./feedback-folder/feedback.txt")
	if err != nil {
		http.Error(w, fmt.Sprint("Error creating feedback.txt", err), http.StatusInternalServerError)
		return
	}
	defer file.Close()
	log.Println("Feedback.txt are created...")
	log.Println("Writing", feedbackText, "into the feedback.txt")

	_, err = fmt.Fprintf(file, feedbackText)
	if err != nil {
		http.Error(w, "Error writing into files", http.StatusInternalServerError)
		return
	}
	log.Println("Redirecting...")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func ProcessResult(w http.ResponseWriter, r *http.Request) {
	// Check directory if the directory exist or not
	log.Println("Checking feedback-folder...")
	if _, err := os.Stat("feedback-folder"); os.IsNotExist(err) {
		http.Error(w, fmt.Sprint("Error while opening feedback-folder:", err.Error()), http.StatusInternalServerError)
		return
	}

	log.Println("Opening feedback.txt")
	file, err := os.Open("./feedback-folder/feedback.txt")
	if err != nil {
		log.Println("feedback.txt does not exist!")
		http.Error(w, fmt.Sprint("feedback.txt does not exist! ", err.Error()), http.StatusInternalServerError)
		return
	}

	feedbackText, err := io.ReadAll(file)
	if err != nil {
		http.Error(w, fmt.Sprint("Error reading file! ", err.Error()), http.StatusInternalServerError)
		return
	}

	log.Println("Rendering feedback to the browser...")
	w.Header().Set("Content-Type", "text/html")
	w.Write(feedbackText)
}
